package com.revolut.testapp.provider.response;

import android.support.annotation.Nullable;
import android.support.v4.util.Pair;

import com.google.gson.annotations.SerializedName;
import com.revolut.testapp.model.Currency;
import com.revolut.testapp.model.RatesBundle;

import java.util.HashMap;
import java.util.Map;

public class RatesBundleResponse {

  public static final RatesBundleResponse EMPTY_RESPONSE = new RatesBundleResponse();

  @SerializedName ("base")
  private String mBaseCurrency;

  @SerializedName ("date")
  private String mDate;

  @SerializedName ("rates")
  private HashMap<String, Double> mRates;

  @Nullable
  public RatesBundle toRatesBundle() {
    Currency baseCurrency = Currency.from(mBaseCurrency);
    if (baseCurrency == null) {
      return null;
    }

    Map<Pair<Currency, Currency>, Double> rates = new HashMap<>(mRates.size());
    for (Map.Entry<String, Double> entry : mRates.entrySet()) {
      Currency cur = Currency.from(entry.getKey());
      if (cur != null) {
        rates.put(Pair.create(baseCurrency, cur), entry.getValue());
      }
    }

    return new RatesBundle(rates);
  }
}
