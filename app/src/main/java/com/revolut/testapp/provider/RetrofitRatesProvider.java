package com.revolut.testapp.provider;

import android.support.annotation.NonNull;

import com.revolut.testapp.model.Currency;
import com.revolut.testapp.model.RatesBundle;
import com.revolut.testapp.provider.response.RatesBundleResponse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;

import static com.revolut.testapp.provider.response.RatesBundleResponse.EMPTY_RESPONSE;

public class RetrofitRatesProvider implements RatesProvider {

  @NonNull
  private final RatesApi mRatesApi;

  @NonNull
  private final List<Currency> mSupportedCurrencies;

  private final int mRequestUpdateTimeoutInSec;

  @Inject
  public RetrofitRatesProvider(@NonNull RatesApi ratesApi,
                               @NonNull Collection<Currency> supportedCurrencies,
                               int requestUpdateTimeoutInSec) {
    mRatesApi = ratesApi;
    mSupportedCurrencies = new ArrayList<>(supportedCurrencies);
    mRequestUpdateTimeoutInSec = requestUpdateTimeoutInSec;
  }

  @Override
  public Observable<RatesBundle> getRatesUpdatesObservable(@NonNull Currency baseCurrency) {
    return Observable
        .interval(0, mRequestUpdateTimeoutInSec, TimeUnit.SECONDS)
        .onBackpressureLatest()   // so we don't generate multiple requests simultaneously (without timeout) in case of long request processing
        .flatMap(i -> mRatesApi.ratesBundle(baseCurrency.mValue)
            .onErrorResumeNext(Observable.just(EMPTY_RESPONSE))
        )
        .filter(response -> response != EMPTY_RESPONSE)
        .map(RatesBundleResponse::toRatesBundle);
  }

  @Override
  public Observable<RatesBundle> getAllRatesUpdatesObservable() {
    return Observable.from(mSupportedCurrencies)
        .flatMap(this::getRatesUpdatesObservable);
  }
}
