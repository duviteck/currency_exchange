package com.revolut.testapp.provider;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.revolut.testapp.model.Currency;
import com.revolut.testapp.model.RatesBundle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;

public class FakeRatesProvider implements RatesProvider {

  private static final int EMIT_TIMEOUT_IN_SEC = 5;

  @NonNull
  private final List<Currency> mSupportedCurrencies;

  @NonNull
  private final Random mRandom;

  @Inject
  public FakeRatesProvider(@NonNull Collection<Currency> supportedCurrencies) {
    mSupportedCurrencies = new ArrayList<>(supportedCurrencies);
    mRandom = new Random();
  }

  @Override
  public Observable<RatesBundle> getRatesUpdatesObservable(@NonNull Currency baseCurrency) {
    return Observable
        .interval(0, EMIT_TIMEOUT_IN_SEC, TimeUnit.SECONDS)
        .map(i -> generateRatesBundle(baseCurrency));
  }

  @Override
  public Observable<RatesBundle> getAllRatesUpdatesObservable() {
    return Observable.from(mSupportedCurrencies)
        .flatMap(this::getRatesUpdatesObservable);
  }

  @NonNull
  private RatesBundle generateRatesBundle(@NonNull Currency baseCurrency) {
    Map<Pair<Currency, Currency>, Double> rates = new HashMap<>(Currency.values().length - 1);

    for (Currency targetCurrency : Currency.values()) {
      if (baseCurrency == targetCurrency) {
        continue;
      }

      double rate = 0.5 + mRandom.nextDouble();
      rates.put(Pair.create(baseCurrency, targetCurrency), rate);
    }

    return new RatesBundle(rates);
  }
}
