package com.revolut.testapp.provider;

import android.support.annotation.NonNull;

import com.revolut.testapp.model.Currency;
import com.revolut.testapp.model.RatesBundle;

import rx.Observable;

public interface RatesProvider {
  Observable<RatesBundle> getRatesUpdatesObservable(@NonNull Currency baseCurrency);
  Observable<RatesBundle> getAllRatesUpdatesObservable();
}
