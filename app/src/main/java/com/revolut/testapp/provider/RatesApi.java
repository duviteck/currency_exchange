package com.revolut.testapp.provider;

import android.support.annotation.NonNull;

import com.revolut.testapp.provider.response.RatesBundleResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface RatesApi {
  @GET ("latest")
  Observable<RatesBundleResponse> ratesBundle(@Query ("base") @NonNull String baseCurrency);
}
