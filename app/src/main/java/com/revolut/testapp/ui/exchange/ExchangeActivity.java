package com.revolut.testapp.ui.exchange;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.revolut.testapp.R;
import com.revolut.testapp.model.Currency;
import com.revolut.testapp.model.RatesBundle;
import com.revolut.testapp.mvp.view.MvpActivity;

import butterknife.BindView;
import rx.Observable;

import static com.revolut.testapp.utils.Utils.DECIMAL_FORMAT_BUDGET;
import static com.revolut.testapp.utils.Utils.DECIMAL_FORMAT_RATE;
import static java.lang.Math.abs;

public class ExchangeActivity extends MvpActivity<ExchangeMvpView, ExchangePresenter>
    implements ExchangeMvpView {

  @BindView (R.id.rate_spinner)
  Spinner mRateSpinner;

  @BindView (R.id.exchange_button)
  TextView mExchangeButton;

  @BindView (R.id.exchange_back_rate)
  TextView mExchangeBackRateView;

  @BindView (R.id.base_currency)
  TextView mBaseCurrencyView;

  @BindView (R.id.base_exchange_amount)
  EditText mBaseExchangeAmountView;

  @BindView (R.id.base_budget_amount)
  TextView mBaseBudgetAmountView;

  @BindView (R.id.target_currency)
  TextView mTargetCurrencyView;

  @BindView (R.id.target_exchange_amount)
  TextView mTargetExchangeAmountView;

  @BindView (R.id.target_budget_amount)
  TextView mTargetBudgetAmountView;

  private ExchangeComponent mComponent;
  private RateSpinnerAdapter mRateSpinnerAdapter;

  @Override
  protected void injectDependencies() {
    mComponent = DaggerExchangeComponent.builder()
        .applicationComponent(getApp().getApplicationComponent())
        .build();
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mRateSpinnerAdapter = new RateSpinnerAdapter(this);
    mRateSpinner.setAdapter(mRateSpinnerAdapter);
    mRateSpinner.setOnItemSelectedListener(mRateSpinnerAdapter);

    mBaseExchangeAmountView.requestFocus();
  }

  @NonNull
  @Override
  public ExchangePresenter createPresenter() {
    return mComponent.getPresenter();
  }

  @Override
  protected int getContentId() {
    return R.layout.activity_exchange;
  }

  @NonNull
  @Override
  protected ExchangeMvpView getMvpView() {
    return this;
  }

  @Override
  @NonNull
  public Observable<Pair<Currency, Currency>> getCurrencyPairObservable() {
    return mRateSpinnerAdapter.getClickObservable()
        .map(exchangeRate ->
            new Pair<>(exchangeRate.getBaseCurrency(), exchangeRate.getTargetCurrency()));
  }

  @NonNull
  public Observable<Double> getBaseExchangeAmountObservable() {
    return RxTextView.textChanges(mBaseExchangeAmountView)
        .map(text -> {
          if (!TextUtils.isEmpty(text)) {
            int dotIndex = text.toString().indexOf('.');
            if (text.length() == 1 && (text.charAt(0) == '-' || text.charAt(0) == '.')) {
              // remove minus ('-') if no digits left
              // don't start input with dot ('.')
              text = null;
              mBaseExchangeAmountView.setText(null);
            } else if (text.charAt(0) != '-') {
              // add minus ('-') if not added yet
              mBaseExchangeAmountView.setText("-" + mBaseExchangeAmountView.getText());
              mBaseExchangeAmountView.setSelection(mBaseExchangeAmountView.getText().length());
            } else if (dotIndex > 0 && text.length() - dotIndex > 3) {
              // keep max of 2 digits after dot sign
              text = text.subSequence(0, dotIndex + 3);
              mBaseExchangeAmountView.setText(text);
              mBaseExchangeAmountView.setSelection(mBaseExchangeAmountView.getText().length());
            }
          }

          return (TextUtils.isEmpty(text)) ? 0d : abs(Float.parseFloat(text.toString()));
        });
  }

  @NonNull
  public Observable<Void> getExchangeRequestObservable() {
    return RxView.clicks(mExchangeButton);
  }

  @Override
  public void onRatesChanged(@NonNull RatesBundle ratesBundle) {
    mRateSpinnerAdapter.updateRates(ratesBundle);
  }

  @Override
  public void onExchangeBackRateChanged(@NonNull Currency baseCur, @NonNull Currency targetCur,
                                        double backRate) {
    mExchangeBackRateView.setText(getString(R.string.exchange_back_rate,
        targetCur.mSymbol, baseCur.mSymbol, DECIMAL_FORMAT_RATE.format(backRate)));
  }

  @Override
  public void onCurrencyPairChanged(@NonNull Currency baseCur, @NonNull Currency targetCur) {
    mBaseCurrencyView.setText(baseCur.mValue);
    mTargetCurrencyView.setText(targetCur.mValue);

    // switch spinner to proper selection
    int selectionPos = mRateSpinnerAdapter.getPosition(baseCur, targetCur);
    if (selectionPos >= 0) {
      mRateSpinner.setSelection(selectionPos);
    }
  }

  @Override
  public void onBaseBudgetAmountChanged(@NonNull Currency baseCur, double amount) {
    mBaseBudgetAmountView.setText(
        getString(R.string.existing_amount, baseCur.mSymbol, DECIMAL_FORMAT_BUDGET.format(amount)));
  }

  @Override
  public void onTargetBudgetAmountChanged(@NonNull Currency targetCur, double amount) {
    mTargetBudgetAmountView.setText(
        getString(R.string.existing_amount, targetCur.mSymbol, DECIMAL_FORMAT_BUDGET.format(amount)));
  }

  @Override
  public void onBaseExchangeAmountCleared() {
    mBaseExchangeAmountView.setText(null);
  }

  @Override
  public void onTargetExchangeAmountChanged(double amount) {
    mTargetExchangeAmountView.setText(
        TextUtils.isEmpty(mBaseExchangeAmountView.getText())
            ? null : '+' + DECIMAL_FORMAT_BUDGET.format(amount));
  }

  @Override
  public void onExchangeAbilityChanged(boolean available) {
    mExchangeButton.setEnabled(available);
  }
}
