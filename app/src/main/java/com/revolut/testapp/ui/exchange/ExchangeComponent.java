package com.revolut.testapp.ui.exchange;

import com.revolut.testapp.dagger.ApplicationComponent;
import com.revolut.testapp.dagger.BaseScope;

import dagger.Component;

@BaseScope
@Component (dependencies = ApplicationComponent.class)
public interface ExchangeComponent {
  ExchangePresenter getPresenter();
}
