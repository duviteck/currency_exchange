package com.revolut.testapp.ui.exchange;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.revolut.testapp.model.Currency;
import com.revolut.testapp.model.RatesBundle;
import com.revolut.testapp.mvp.view.MvpView;

import rx.Observable;

public interface ExchangeMvpView extends MvpView {
  Observable<Pair<Currency, Currency>> getCurrencyPairObservable();
  Observable<Double> getBaseExchangeAmountObservable();
  Observable<Void> getExchangeRequestObservable();

  void onRatesChanged(@NonNull RatesBundle ratesBundle);
  void onExchangeBackRateChanged(@NonNull Currency baseCur, @NonNull Currency targetCur,
                                 double backRate);
  void onCurrencyPairChanged(@NonNull Currency baseCur, @NonNull Currency targetCur);
  void onBaseBudgetAmountChanged(@NonNull Currency baseCur, double amount);
  void onTargetBudgetAmountChanged(@NonNull Currency targetCur, double amount);
  void onBaseExchangeAmountCleared();
  void onTargetExchangeAmountChanged(double amount);
  void onExchangeAbilityChanged(boolean available);
}
