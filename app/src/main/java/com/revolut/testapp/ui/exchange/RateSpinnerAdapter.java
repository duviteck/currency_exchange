package com.revolut.testapp.ui.exchange;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.revolut.testapp.R;
import com.revolut.testapp.model.Currency;
import com.revolut.testapp.model.ExchangeRate;
import com.revolut.testapp.model.RatesBundle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;

import static com.revolut.testapp.utils.Utils.DECIMAL_FORMAT_RATE;
import static com.revolut.testapp.utils.Utils.dpToPx;

class RateSpinnerAdapter extends BaseAdapter implements AdapterView.OnItemSelectedListener {

  private final PublishSubject<ExchangeRate> mClickSubject;

  @NonNull
  private final List<ExchangeRate> mRates;

  @NonNull
  private final Context mContext;

  @NonNull
  private final LayoutInflater mInflater;

  private final int mDropdownVerticalPadding;

  private int mSelectedPos;

  RateSpinnerAdapter(@NonNull Context context) {
    mClickSubject = PublishSubject.create();
    mRates = new ArrayList<>();
    mContext = context;
    mInflater = LayoutInflater.from(context);

    mDropdownVerticalPadding = dpToPx(
        context.getResources().getDimension(R.dimen.exchange_rate_dropdown_vertical_padding));
  }

  @Override
  public int getCount() {
    return mRates.size();
  }

  @Override
  public ExchangeRate getItem(int position) {
    return mRates.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    return getViewInner(position, convertView, parent, true);
  }

  @Override
  public View getDropDownView(int position, View convertView, ViewGroup parent) {
    return getViewInner(position, convertView, parent, false);
  }

  @NonNull
  private View getViewInner(int position, View convertView, ViewGroup parent, boolean isMainView) {
    ViewHolder holder;
    if (convertView == null) {
      convertView = mInflater.inflate(R.layout.exchange_rate_list_item, parent, false);
      holder = new ViewHolder();
      holder.text = (TextView) convertView.findViewById(R.id.text);
      convertView.setTag(holder);
      if (!isMainView) {
        holder.text.setBackgroundResource(R.color.bottom_exchange_color);
        holder.text.setPadding(0, mDropdownVerticalPadding, 0, mDropdownVerticalPadding);
      }
    } else {
      holder = (ViewHolder) convertView.getTag();
    }

    ExchangeRate exchangeRate = getItem(position);
    holder.text.setText(mContext.getString(R.string.exchange_rate,
        exchangeRate.getBaseCurrency().mSymbol,
        exchangeRate.getTargetCurrency().mSymbol,
        DECIMAL_FORMAT_RATE.format(exchangeRate.getRate())));
    if (!isMainView) {
      boolean isSelected = position == mSelectedPos;
      holder.text.setTextColor(isSelected ? Color.BLACK : Color.WHITE);
      holder.text.setBackgroundResource(isSelected ? R.color.white_light : R.color.bottom_exchange_color);
    }

    return convertView;
  }

  void updateRates(@NonNull RatesBundle newRates) {
    ExchangeRate oldSelection = mRates.size() > mSelectedPos ? mRates.get(mSelectedPos) : null;

    mRates.clear();
    mRates.addAll(newRates.getAllExchangeRates());
    Collections.sort(mRates, ExchangeRate.COMPARATOR);

    if (oldSelection != null) {
      mSelectedPos = getPosition(oldSelection.getBaseCurrency(), oldSelection.getTargetCurrency());
    }

    notifyDataSetChanged();
  }

  int getPosition(@NonNull Currency baseCurrency, @NonNull Currency targetCurrency) {
    for (int i = 0; i < mRates.size(); i++) {
      ExchangeRate rate = mRates.get(i);
      if (rate.getBaseCurrency() == baseCurrency && rate.getTargetCurrency() == targetCurrency) {
        return i;
      }
    }

    return -1;
  }

  @NonNull
  public Observable<ExchangeRate> getClickObservable() {
    return mClickSubject.asObservable();
  }

  @Override
  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    mSelectedPos = position;
    mClickSubject.onNext(getItem(position));
  }

  @Override
  public void onNothingSelected(AdapterView<?> parent) {
  }

  private final static class ViewHolder {
    TextView text;
  }
}
