package com.revolut.testapp.ui.exchange;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.revolut.testapp.model.Budget;
import com.revolut.testapp.model.Currency;
import com.revolut.testapp.model.RatesBundle;
import com.revolut.testapp.mvp.presenter.BasePresenter;
import com.revolut.testapp.preferences.ApplicationPreferences;
import com.revolut.testapp.preferences.data.BudgetProvider;
import com.revolut.testapp.provider.RatesProvider;
import com.revolut.testapp.utils.Logger;
import com.revolut.testapp.utils.RxUtils;

import java.util.Collection;

import javax.inject.Inject;

import static com.revolut.testapp.model.Budget.BUDGET_PRECISION;
import static com.revolut.testapp.model.RatesBundle.RATE_PRECISION;
import static com.revolut.testapp.utils.Utils.doubleWithPrec;

public class ExchangePresenter extends BasePresenter<ExchangeMvpView> {

  @NonNull
  private final RatesProvider mRatesProvider;

  @NonNull
  private final BudgetProvider mBudgetProvider;

  @NonNull
  private RatesBundle mRatesBundle;

  @NonNull
  private Currency mBaseCurrency;

  @NonNull
  private Currency mTargetCurrency;

  @Nullable
  private Budget mBudget;

  private double mBaseExchangeAmount;

  @Inject
  public ExchangePresenter(@NonNull RatesProvider ratesProvider,
                           @NonNull ApplicationPreferences preferences,
                           @NonNull Collection<Currency> supportedCurrencies) {
    mRatesProvider = ratesProvider;
    mBudgetProvider = preferences.getBudgetValue();
    mRatesBundle = RatesBundle.getAllCurrenciesEmptyBundle(supportedCurrencies);

    mBaseCurrency = Currency.USD;
    mTargetCurrency = Currency.EUR;
  }

  @Override
  public void attachView(@NonNull ExchangeMvpView view) {
    super.attachView(view);

    bind(mRatesProvider.getAllRatesUpdatesObservable()
        .compose(RxUtils.async())
        .doOnNext(ratesBundle -> Logger.d("new [rates]: %s", ratesBundle))
        .subscribe(ratesBundle -> {
          mRatesBundle.merge(ratesBundle);
          view.onRatesChanged(mRatesBundle);

          if (ratesBundle.hasRate(mBaseCurrency, mTargetCurrency)) {
            updateExchangeBackRate();
            view.onTargetExchangeAmountChanged(getTargetExchangeAmount());
          }
        }));

    bind(view.getCurrencyPairObservable()
        .doOnNext(currencyPair -> Logger.d("new [currency pair]: %s", currencyPair))
        .subscribe(currencyPair -> {
          mBaseCurrency = currencyPair.first;
          mTargetCurrency = currencyPair.second;

          view.onCurrencyPairChanged(mBaseCurrency, mTargetCurrency);
          view.onBaseExchangeAmountCleared();
          view.onTargetExchangeAmountChanged(0d);

          if (mBudget != null) {
            updateBudget(mBudget);
          }

          updateExchangeBackRate();
        })
    );

    bind(view.getBaseExchangeAmountObservable()
        .doOnNext(amount -> Logger.d("new exchange [amount]: %f", amount))
        .subscribe(amount -> {
          mBaseExchangeAmount = doubleWithPrec(amount, BUDGET_PRECISION);
          view.onTargetExchangeAmountChanged(getTargetExchangeAmount());
          view.onExchangeAbilityChanged(isExchangeAvailable());
        })
    );

    bind(view.getExchangeRequestObservable()
        .doOnNext(aVoid -> Logger.d("requested [exchange]"))
        .subscribe(
            aVoid -> {
              exchange(mBaseCurrency, mBaseExchangeAmount, mTargetCurrency, getTargetExchangeAmount());
              view.onBaseExchangeAmountCleared();
            }
        )
    );

    bind(mBudgetProvider.asObservable()
        .doOnNext(budget -> Logger.d("new [budget]: %s", budget))
        .subscribe(this::updateBudget)
    );

    view.onRatesChanged(mRatesBundle);
    view.onCurrencyPairChanged(mBaseCurrency, mTargetCurrency);
    updateBudget(mBudgetProvider.get());
    updateExchangeBackRate();
  }

  private void exchange(@NonNull Currency baseCurrency, double baseAmount,
                        @NonNull Currency targetCurrency, double targetAmount) {
    if (mBudget != null) {
      mBudget.reduce(baseCurrency, baseAmount);
      mBudget.increase(targetCurrency, targetAmount);
      mBudgetProvider.set(mBudget);
    }
  }

  private double getTargetExchangeAmount() {
    double targetAmount = mBaseExchangeAmount * mRatesBundle.get(mBaseCurrency, mTargetCurrency);
    return doubleWithPrec(targetAmount, BUDGET_PRECISION);
  }

  private void updateBudget(@NonNull Budget budget) {
    mBudget = budget;
    ExchangeMvpView view = getView();
    if (view != null) {
      view.onBaseBudgetAmountChanged(mBaseCurrency, mBudget.getAmount(mBaseCurrency));
      view.onTargetBudgetAmountChanged(mTargetCurrency, mBudget.getAmount(mTargetCurrency));
    }
  }

  private void updateExchangeBackRate() {
    ExchangeMvpView view = getView();
    if (view != null) {
      double rate = mRatesBundle.get(mBaseCurrency, mTargetCurrency);
      view.onExchangeBackRateChanged(
          mBaseCurrency, mTargetCurrency, doubleWithPrec(1d / rate, RATE_PRECISION));
    }
  }

  private boolean isExchangeAvailable() {
    return mBudget != null
        && mBaseExchangeAmount > 0
        && mBaseExchangeAmount <= mBudget.getAmount(mBaseCurrency)
        && mRatesBundle.hasRate(mBaseCurrency, mTargetCurrency);
  }
}
