package com.revolut.testapp.utils;

import android.content.res.Resources;
import android.util.DisplayMetrics;

import com.revolut.testapp.model.Budget;
import com.revolut.testapp.model.RatesBundle;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import static android.text.TextUtils.join;
import static java.lang.Double.NaN;
import static java.util.Collections.nCopies;

public class Utils {

  public static final DecimalFormat DECIMAL_FORMAT_BUDGET = new DecimalFormat(
      "0." + join("", nCopies(Budget.BUDGET_PRECISION, '#')),
      new DecimalFormatSymbols(Locale.US));

  public static final DecimalFormat DECIMAL_FORMAT_RATE = new DecimalFormat(
      "0." + join("", nCopies(RatesBundle.RATE_PRECISION, '#')),
      new DecimalFormatSymbols(Locale.US));

  public static double doubleWithPrec(double value, int precision) {
    return Double.isNaN(value)
        ? NaN : BigDecimal.valueOf(value).setScale(precision, RoundingMode.HALF_UP).doubleValue();
  }

  public static int dpToPx(float dp){
    DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
    float px = dp * (metrics.densityDpi / 160f);
    return Math.round(px);
  }
}
