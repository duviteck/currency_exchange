package com.revolut.testapp;

import android.app.Application;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDelegate;

import com.revolut.testapp.dagger.ApplicationComponent;
import com.revolut.testapp.dagger.ApplicationModule;
import com.revolut.testapp.dagger.DaggerApplicationComponent;
import com.revolut.testapp.utils.Logger;

import rx.plugins.RxJavaHooks;

public class RevolutApplication extends Application {
  private ApplicationComponent mApplicationComponent;

  @Override
  public void onCreate() {
    super.onCreate();

    AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    RxJavaHooks.setOnError(Logger::ex);
    if (BuildConfig.DEBUG) {
      StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
          .detectAll()
          .penaltyLog()
          .build()
      );
      StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
          .detectAll()
          .detectActivityLeaks()
          .detectLeakedClosableObjects()
          .penaltyLog()
          .build()
      );
    }

    mApplicationComponent = createApplicationComponent();
  }

  @NonNull
  protected ApplicationComponent createApplicationComponent() {
    return DaggerApplicationComponent.builder()
        .applicationModule(new ApplicationModule(this))
        .build();
  }

  @NonNull
  public ApplicationComponent getApplicationComponent() {
    return mApplicationComponent;
  }
}
