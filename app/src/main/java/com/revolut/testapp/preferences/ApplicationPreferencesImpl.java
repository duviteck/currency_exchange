package com.revolut.testapp.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.revolut.testapp.RevolutApplication;
import com.revolut.testapp.model.Budget;
import com.revolut.testapp.preferences.data.BudgetProvider;

public class ApplicationPreferencesImpl implements ApplicationPreferences {
  private static final String PREFERENCES_NAME = "RevolutPrefs";
  private static final String KEY_BUDGET = "key_budget";

  @NonNull
  private final PreferencesDelegate mPrefsDelegate;

  public ApplicationPreferencesImpl(@NonNull RevolutApplication app) {
    SharedPreferences sharedPrefs = app.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    mPrefsDelegate = new PreferencesDelegate(sharedPrefs);
  }

  @NonNull
  @Override
  public BudgetProvider getBudgetValue() {
    return new BudgetProvider(mPrefsDelegate, KEY_BUDGET, Budget.createRandomBudget());
  }
}
