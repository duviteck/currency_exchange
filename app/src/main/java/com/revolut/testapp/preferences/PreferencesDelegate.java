package com.revolut.testapp.preferences;

import android.content.SharedPreferences;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import rx.Observable;
import rx.subscriptions.Subscriptions;

public class PreferencesDelegate {
  private static final String DELIMITER = "#@!";

  @NonNull
  private final SharedPreferences mPrefs;

  @NonNull
  private final Observable<String> mKeyChanges;

  PreferencesDelegate(@NonNull SharedPreferences prefs) {
    mPrefs = prefs;
    mKeyChanges = Observable.create(
        (Observable.OnSubscribe<String>) subscriber -> {
          final SharedPreferences.OnSharedPreferenceChangeListener listener =
              (preferences, key) -> subscriber.onNext(key);
          mPrefs.registerOnSharedPreferenceChangeListener(listener);
          subscriber.add(Subscriptions.create(
              () -> mPrefs.unregisterOnSharedPreferenceChangeListener(listener)));
        }).share();
  }

  public int getInt(@NonNull String key, int defaultValue) {
    return mPrefs.getInt(key, defaultValue);
  }

  public void putInt(@NonNull String key, int value) {
    mPrefs.edit().putInt(key, value).apply();
  }

  @NonNull
  public String getString(@NonNull String key, @NonNull String defaultValue) {
    return mPrefs.getString(key, defaultValue);
  }

  public void putString(@NonNull String key, @NonNull String value) {
    mPrefs.edit().putString(key, value).apply();
  }

  public boolean getBoolean(@NonNull String key, boolean defaultValue) {
    return mPrefs.getBoolean(key, defaultValue);
  }

  public void putBoolean(@NonNull String key, boolean value) {
    mPrefs.edit().putBoolean(key, value).apply();
  }

  public long getLong(@NonNull String key, long defaultValue) {
    return mPrefs.getLong(key, defaultValue);
  }

  public void putLong(@NonNull String key, long value) {
    mPrefs.edit().putLong(key, value).apply();
  }

  public double getDouble(@NonNull String key, double defaultValue) {
    throw new IllegalArgumentException("Unsupported type 'double'");
  }

  public void putDouble(@NonNull String key, double value) {
    throw new IllegalArgumentException("Unsupported type 'double'");
  }

  public float getFloat(@NonNull String key, float defaultValue) {
    return mPrefs.getFloat(key, defaultValue);
  }

  public void putFloat(@NonNull String key, float value) {
    mPrefs.edit().putFloat(key, value).apply();
  }

  @NonNull
  public Set<String> getStringSet(@NonNull String key) {
    return mPrefs.getStringSet(key, Collections.<String>emptySet());
  }

  public void putStringSet(@NonNull String key, @NonNull Set<String> value) {
    mPrefs.edit().putStringSet(key, value).apply();
  }

  @NonNull
  public List<String> getStringList(@NonNull String key, @NonNull List<String> defaultValue) {
    final String value = mPrefs.getString(key, null);
    return value == null ? defaultValue : Arrays.asList(value.split(DELIMITER));
  }

  public void putStringList(@NonNull String key, @NonNull List<String> value) {
    mPrefs.edit().putString(key, TextUtils.join(DELIMITER, value)).apply();
  }

  public <T extends Parcelable> T getParcelable(@NonNull String key) {
    throw new IllegalArgumentException("Unsupported type 'Parcelable'");
  }

  public <T extends Parcelable> void putParcelable(@NonNull String key, @NonNull T value) {
    throw new IllegalArgumentException("Unsupported type 'Parcelable'");
  }

  public void remove(String key) {
    mPrefs.edit().remove(key).apply();
  }

  public void clear() {
    mPrefs.edit().clear().apply();
  }

  @NonNull
  public Observable<String> keyChanges() {
    return mKeyChanges;
  }
}
