package com.revolut.testapp.preferences.data;

import android.support.annotation.NonNull;

import com.revolut.testapp.preferences.PreferencesDelegate;

import rx.Observable;

public abstract class TypedValue<T> {

  private final PreferencesDelegate mDelegate;

  private final String mKey;
  private final T mDefaultValue;

  TypedValue(@NonNull PreferencesDelegate delegate, @NonNull String key, @NonNull T defaultValue) {
    mDelegate = delegate;
    mKey = key;
    mDefaultValue = defaultValue;
  }

  @NonNull
  PreferencesDelegate getDelegate() {
    return mDelegate;
  }

  @NonNull
  public String getKey() {
    return mKey;
  }

  public void remove() {
    getDelegate().remove(getKey());
  }

  public T get() {
    return get(mDefaultValue);
  }

  public abstract void set(T data);

  @NonNull
  public Observable<T> asObservable() {
    return getDelegate().keyChanges()
        .filter(getKey()::equals)
        .map(key -> get());
  }

  public abstract T get(@NonNull T defaultValue);
}
