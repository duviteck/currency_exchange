package com.revolut.testapp.preferences.data;

import android.support.annotation.NonNull;

import com.revolut.testapp.model.Budget;
import com.revolut.testapp.model.Currency;
import com.revolut.testapp.preferences.PreferencesDelegate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;

public class BudgetProvider extends TypedValue<Budget> {
  private static final String POSTFIX_SERIALIZED_BUDGET = ".serialized_budget";

  public BudgetProvider(@NonNull PreferencesDelegate delegate, @NonNull String key,
                        @NonNull Budget defaultValue) {
    super(delegate, key, defaultValue);
  }

  @Override
  public void set(Budget budget) {
    Map<Currency, Double> budgetByCurrencies = budget.getBudgetByCurrencies();
    List<String> serializedBudget = new ArrayList<>(budgetByCurrencies.size() * 2);

    for (Map.Entry<Currency, Double> amountInCurrency : budgetByCurrencies.entrySet()) {
      serializedBudget.add(amountInCurrency.getKey().mValue);
      serializedBudget.add(amountInCurrency.getValue().toString());
    }

    final PreferencesDelegate delegate = getDelegate();
    final String key = getKey();
    delegate.putBoolean(key, true);
    delegate.putStringList(key + POSTFIX_SERIALIZED_BUDGET, serializedBudget);
  }

  @NonNull
  @Override
  public Observable<Budget> asObservable() {
    return getDelegate().keyChanges()
        .filter(changedKey -> changedKey.equals(getKey() + POSTFIX_SERIALIZED_BUDGET))
        .map(key -> get());
  }

  @Override
  public Budget get(@NonNull Budget defaultValue) {
    final PreferencesDelegate delegate = getDelegate();
    final String key = getKey();

    if (!delegate.getBoolean(key, false)) {
      return defaultValue;
    }

    List<String> serializedBudget =
        delegate.getStringList(key + POSTFIX_SERIALIZED_BUDGET, Collections.emptyList());
    Map<Currency, Double> budgetInCurrencies = new HashMap<>(serializedBudget.size() / 2);

    for (int i = 0; i < serializedBudget.size(); i += 2) {
      Currency currency = Currency.from(serializedBudget.get(i));
      Double amount = Double.parseDouble(serializedBudget.get(i + 1));
      budgetInCurrencies.put(currency, amount);
    }

    return new Budget(budgetInCurrencies);
  }
}
