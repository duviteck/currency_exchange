package com.revolut.testapp.preferences;

import android.support.annotation.NonNull;

import com.revolut.testapp.preferences.data.BudgetProvider;

public interface ApplicationPreferences {
  @NonNull
  BudgetProvider getBudgetValue();
}
