package com.revolut.testapp.model;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.revolut.testapp.utils.Utils.doubleWithPrec;

public class RatesBundle {

  public static final int RATE_PRECISION = 4;

  @NonNull
  private final Map<Pair<Currency, Currency>, Double> mRates = new HashMap<>();

  private RatesBundle() {
  }

  @NonNull
  public static RatesBundle getAllCurrenciesEmptyBundle(@NonNull Collection<Currency> currencies) {
    RatesBundle result = new RatesBundle();

    for (Currency baseCur : currencies) {
      for (Currency targetCur : currencies) {
        if (baseCur == targetCur) {
          continue;
        }

        result.mRates.put(Pair.create(baseCur, targetCur), Double.NaN);
      }
    }

    return result;
  }

  public RatesBundle(@NonNull Map<Pair<Currency, Currency>, Double> rates) {
    mRates.putAll(rates);
  }

  public void merge(@NonNull RatesBundle ratesBundle) {
    mRates.putAll(ratesBundle.mRates);
  }

  public double get(@NonNull Currency baseCurrency, @NonNull Currency targetCurrency) {
    Double result = mRates.get(Pair.create(baseCurrency, targetCurrency));
    return result != null ? doubleWithPrec(result, RATE_PRECISION) : Double.NaN;
  }

  public boolean hasRate(@NonNull Currency baseCurrency, @NonNull Currency targetCurrency) {
    Double result = mRates.get(Pair.create(baseCurrency, targetCurrency));
    return result != null && !Double.isNaN(result);
  }

  @NonNull
  public List<ExchangeRate> getAllExchangeRates() {
    List<ExchangeRate> allRates = new ArrayList<>(mRates.size());
    for (Map.Entry<Pair<Currency, Currency>, Double> entry : mRates.entrySet()) {
      allRates.add(new ExchangeRate(
          entry.getKey().first, entry.getKey().second, doubleWithPrec(entry.getValue(), RATE_PRECISION)));
    }

    return allRates;
  }

  @Override
  public String toString() {
    return "RatesBundle{" + mRates + '}';
  }
}
