package com.revolut.testapp.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Comparator;

public enum Currency {
  USD("USD", "$", 0),
  EUR("EUR", "€", 1),
  GBP("GBP", "£", 2);

  public static final Comparator<Currency> COMPARATOR =
      (c1, c2) -> Integer.compare(c1.mOrder, c2.mOrder);

  @NonNull
  public final String mValue;

  @NonNull
  public final String mSymbol;

  private final int mOrder;

  Currency(@NonNull String value, @NonNull String symbol, int order) {
    mValue = value;
    mSymbol = symbol;
    mOrder = order;
  }

  @Nullable
  public static Currency from(@Nullable String currencyValue) {
    if (currencyValue == null) {
      return null;
    }

    for (Currency currency : values()) {
      if (currency.mValue.equals(currencyValue)) {
        return currency;
      }
    }

    return null;
  }
}
