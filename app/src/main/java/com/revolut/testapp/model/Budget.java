package com.revolut.testapp.model;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.revolut.testapp.utils.Utils.doubleWithPrec;

public class Budget {

  public static final int BUDGET_PRECISION = 2;

  @NonNull
  private final Map<Currency, Double> mBudgetByCurrencies = new HashMap<>();

  @NonNull
  public static Budget createEmptyBudget() {
    return new Budget(Collections.emptyMap());
  }

  @NonNull
  public static Budget createRandomBudget() {
    Random random = new Random();
    Map<Currency, Double> budgetInCurrencies = new HashMap<>(3);
    budgetInCurrencies.put(
        Currency.USD, doubleWithPrec(80d + 40 * random.nextDouble(), BUDGET_PRECISION));
    budgetInCurrencies.put(
        Currency.EUR, doubleWithPrec(80d + 40 * random.nextDouble(), BUDGET_PRECISION));
    budgetInCurrencies.put(
        Currency.GBP, doubleWithPrec(80d + 40 * random.nextDouble(), BUDGET_PRECISION));
    return new Budget(budgetInCurrencies);
  }

  public Budget(@NonNull Map<Currency, Double> budgetByCurrencies) {
    mBudgetByCurrencies.putAll(budgetByCurrencies);
  }

  @NonNull
  public Map<Currency, Double> getBudgetByCurrencies() {
    return mBudgetByCurrencies;
  }

  public double getAmount(@NonNull Currency currency) {
    Double amount = mBudgetByCurrencies.get(currency);
    return amount == null ? 0d : doubleWithPrec(amount, BUDGET_PRECISION);
  }

  public void increase(@NonNull Currency currency, double amount) {
    Double curAmount = mBudgetByCurrencies.get(currency);
    double newAmount = amount + (curAmount == null ? 0 : curAmount);
    mBudgetByCurrencies.put(currency, doubleWithPrec(newAmount, BUDGET_PRECISION));
  }

  public void reduce(@NonNull Currency currency, double amount) {
    double curAmount = mBudgetByCurrencies.containsKey(currency)
        ? mBudgetByCurrencies.get(currency) : 0;
    if (curAmount < amount) {
      throw new IllegalArgumentException("New amount can't be negative");
    }

    mBudgetByCurrencies.put(currency, doubleWithPrec(curAmount - amount, BUDGET_PRECISION));
  }

  @Override
  public String toString() {
    return "Budget{" + mBudgetByCurrencies + '}';
  }
}
