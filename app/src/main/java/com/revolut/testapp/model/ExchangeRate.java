package com.revolut.testapp.model;

import android.support.annotation.NonNull;

import java.util.Comparator;

public class ExchangeRate {

  public static final Comparator<ExchangeRate> COMPARATOR =
      (r1, r2) -> {
        if (r1.mBaseCurrency != r2.mBaseCurrency) {
          return Currency.COMPARATOR.compare(r1.mBaseCurrency, r2.mBaseCurrency);
        } else if (r1.mTargetCurrency != r2.mTargetCurrency) {
          return Currency.COMPARATOR.compare(r1.mTargetCurrency, r2.mTargetCurrency);
        } else {
          return 0;
        }
      };

  @NonNull
  private final Currency mBaseCurrency;

  @NonNull
  private final Currency mTargetCurrency;

  private final double mRate;

  public ExchangeRate(@NonNull Currency base, @NonNull Currency target, double rate) {
    mBaseCurrency = base;
    mTargetCurrency = target;
    mRate = rate;
  }

  public Currency getBaseCurrency() {
    return mBaseCurrency;
  }

  public Currency getTargetCurrency() {
    return mTargetCurrency;
  }

  public double getRate() {
    return mRate;
  }
}
