package com.revolut.testapp.dagger;

import com.revolut.testapp.RevolutApplication;
import com.revolut.testapp.model.Currency;
import com.revolut.testapp.preferences.ApplicationPreferences;
import com.revolut.testapp.provider.RatesProvider;

import java.util.Collection;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component (modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {
  RevolutApplication getApplication();
  ApplicationPreferences getPreferences();
  RatesProvider getRatesProvider();
  Collection<Currency> getSupportedCurrencies();
  int getRequestUpdateTimeoutInSec();
}
