package com.revolut.testapp.dagger;

import android.support.annotation.NonNull;

import com.revolut.testapp.RevolutApplication;
import com.revolut.testapp.model.Currency;
import com.revolut.testapp.preferences.ApplicationPreferences;
import com.revolut.testapp.preferences.ApplicationPreferencesImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
  @NonNull
  private final RevolutApplication mApplication;

  public ApplicationModule(@NonNull RevolutApplication application) {
    mApplication = application;
  }

  @Singleton
  @Provides
  public RevolutApplication provideApplication() {
    return mApplication;
  }

  @Singleton
  @Provides
  public ApplicationPreferences provideApplicationPreferences(RevolutApplication application) {
    return new ApplicationPreferencesImpl(application);
  }

  @Singleton
  @Provides
  public Collection<Currency> provideSupportedCurrencies() {
    return new ArrayList<>(Arrays.asList(Currency.values()));
  }
}
