package com.revolut.testapp.dagger;

import com.google.gson.Gson;
import com.revolut.testapp.BuildConfig;
import com.revolut.testapp.model.Currency;
import com.revolut.testapp.provider.RatesApi;
import com.revolut.testapp.provider.RatesProvider;
import com.revolut.testapp.provider.RetrofitRatesProvider;
import com.revolut.testapp.provider.retrofit.RxErrorHandlingCallAdapterFactory;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {
  private static final int CONNECT_TIMEOUT = 10;
  private static final int WRITE_TIMEOUT = 120;
  private static final int READ_TIMEOUT = 60;

  private static final int REQUEST_UPDATE_TIMEOUT_IN_SEC = 30;

  @Provides
  @Singleton
  public OkHttpClient provideHttpClient() {
    final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
    loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
    return new OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor)
        .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
        .build();
  }

  @Provides
  @Singleton
  public Retrofit provideRetrofit(OkHttpClient httpClient) {
    return new Retrofit.Builder()
        .baseUrl(BuildConfig.RATES_URL)
        .client(httpClient)
        .addConverterFactory(GsonConverterFactory.create(new Gson()))
        .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
        .build();
  }

  @Provides
  @Singleton
  public RatesApi providesRatesApi(Retrofit retrofit) {
    return retrofit.create(RatesApi.class);
  }

  @Provides
  @Singleton
  public RatesProvider provideRatesProvider(RatesApi api, Collection<Currency> supportedCurrencies,
                                            int requestUpdateTimeoutInSec) {
    return new RetrofitRatesProvider(api, supportedCurrencies, requestUpdateTimeoutInSec);
//    return new FakeRatesProvider(supportedCurrencies);
  }

  @Provides
  public int provideRequestUpdateTimeoutInSec() {
    return REQUEST_UPDATE_TIMEOUT_IN_SEC;
  }
}
