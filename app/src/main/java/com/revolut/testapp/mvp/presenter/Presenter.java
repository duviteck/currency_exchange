package com.revolut.testapp.mvp.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.UiThread;

import com.revolut.testapp.mvp.view.MvpView;

public interface Presenter<V extends MvpView> {
  @UiThread
  void attachView(@NonNull V view);

  @UiThread
  void detachView();
}
