package com.revolut.testapp.mvp.view;

import com.revolut.testapp.mvp.presenter.Presenter;

class ActivityMvpNonConfigurationInstances<V extends MvpView, P extends Presenter<V>> {
  final P presenter;
  final Object externalInstance;

  ActivityMvpNonConfigurationInstances(P presenter, Object externalInstance) {
    this.presenter = presenter;
    this.externalInstance = externalInstance;
  }
}
